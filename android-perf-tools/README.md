# android-perf-tools

These images are using when performance testing android devices. They contain
common tools and requirements for running [web page replay](https://chromium.googlesource.com/catapult/+/HEAD/web_page_replay_go/README.md)
, [adb](https://developer.android.com/studio/command-line/adb) and nodejs (for the [lighthouse framework](https://developers.google.com/web/tools/lighthouse))
