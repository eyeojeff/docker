# Copyright (c) 2020-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
FROM ubuntu:18.04

# Settings used later on in Dockerfile.
ARG CI_USER="ci_user"
ARG WORK_DIR="/opt/ci"
ARG HOME="/home/ci_user"
ARG TIMEZONE="Europe/Berlin"

# Adjust env.
ENV LC_CTYPE="en_US.UTF-8"

# Set timezone.
RUN ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime && echo $TIMEZONE > /etc/timezone

# Install generic system packages.
# Project specific dependencies should be added by child-Dockerfiles
RUN echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections \
&&  apt-get update \
&&  apt-get install -qy --no-install-recommends \
        build-essential \
        ca-certificates \
        ccache \
        curl \
        dumb-init \
        git \
        lsb-core \
        p7zip-full \
        python \
        sudo \
        unzip \
        wget \
&&  rm -rf /var/lib/apt/lists/*

# Install gitlab runner binary, we dont need anything else from the package
# https://docs.gitlab.com/runner/install/linux-manually.html#install-1
RUN curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64 \
&&  chmod +x /usr/local/bin/gitlab-runner

# Create and configure a CI user with full sudo access
# (install scripts try to install packages so need sudo without password)
RUN adduser --gecos "" --disabled-password $CI_USER --home $HOME \
&&  usermod -aG sudo $CI_USER \
&&  echo "$CI_USER ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers \
&&  mkdir $WORK_DIR \
&&  chown -R $CI_USER:$CI_USER $WORK_DIR

# By default SIGTERM is used which ungracefully kills all
# running builds. Use SIGQUIT instead
STOPSIGNAL SIGQUIT
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["gitlab-runner", "run", "--working-directory", "/opt/ci", "--user", "ci_user"]
