# Copyright (c) 2020-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM ubuntu:18.04

# Settings used later on in Dockerfile.
ARG WORK_DIR="/opt/ci"
ARG TIMEZONE="Europe/Berlin"

# Adjust env.
ENV LC_CTYPE="en_US.UTF-8"

# Set timezone and create our workdir
RUN ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime \
&&  echo $TIMEZONE > /etc/timezone \
&&  mkdir $WORK_DIR

# Install generic system packages
RUN echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections \
&&  apt-get update \
&&  apt-get install -qy --no-install-recommends \
        build-essential \
        ca-certificates \
        ccache \
        curl \
        dumb-init \
        git \
        lsb-core \
        sudo \
        unzip \
        wget \
        ruby-full \
        npm \
        openjdk-11-jdk \
        # Emulator & video bridge dependencies
        libc6 libdbus-1-3 libfontconfig1 libgcc1 \
        libpulse0 libtinfo5 libx11-6 libxcb1 libxdamage1 \
        libnss3 libxcomposite1 libxcursor1 libxi6 \
        libxext6 libxfixes3 zlib1g libgl1 pulseaudio socat \
&&  rm -rf /var/lib/apt/lists/*

# Install bundler gem
RUN gem install bundler

# Install Android SDK tools
# source https://developer.android.com/studio#downloads
ARG ANDROID_TOOLS_URL="https://dl.google.com/android/repository/commandlinetools-linux-6858069_latest.zip"
ARG ANDROID_TOOLS_SHA256_CHECKSUM="87f6dcf41d4e642e37ba03cb2e387a542aa0bd73cb689a9e7152aad40a6e7a08"
ARG ANDROID_NDK_VERSION="21.1.6352462"

ENV ANDROID_HOME=/opt/android-sdk
ENV ANDROID_SDK_ROOT ${ANDROID_HOME}
ENV ANDROID_NDK_HOME=${ANDROID_HOME}/ndk/${ANDROID_NDK_VERSION}
ENV ANDROID_NDK_ROOT=${ANDROID_NDK_HOME}

ENV ADB_INSTALL_TIMEOUT 120
RUN mkdir ${ANDROID_SDK_ROOT} \
&&  wget --quiet ${ANDROID_TOOLS_URL} -O /tmp/sdk-tools.zip \
&&  echo "${ANDROID_TOOLS_SHA256_CHECKSUM} /tmp/sdk-tools.zip" | sha256sum -c \
&&  unzip -q /tmp/sdk-tools.zip -d ${ANDROID_SDK_ROOT}/cmdline-tools \
&&  rm /tmp/sdk-tools.zip \
&&  mv ${ANDROID_SDK_ROOT}/cmdline-tools/cmdline-tools ${ANDROID_SDK_ROOT}/cmdline-tools/latest

ENV PATH=${ANDROID_SDK_ROOT}/emulator:${ANDROID_SDK_ROOT}/cmdline-tools/latest/bin:${ANDROID_SDK_ROOT}/tools:${ANDROID_SDK_ROOT}/tools/bin:${ANDROID_SDK_ROOT}/platform-tools:${PATH}

RUN mkdir ~/.android && echo '### User Sources for Android SDK Manager' > ~/.android/repositories.cfg

# Accept licenses and update SDK manager
RUN yes | sdkmanager --licenses && yes | sdkmanager --update
RUN sdkmanager "tools"

# Configure emulator
ARG DEVICE_PACKAGE="system-images;android-30;google_apis;x86_64"
RUN sdkmanager --install $DEVICE_PACKAGE
RUN avdmanager --verbose create avd --name "device-android-30" --package $DEVICE_PACKAGE --device "Nexus 5"

# Add emulator scripts
ADD start-emulator.sh ${ANDROID_SDK_ROOT}/emulator
ADD stop-emulator.sh ${ANDROID_SDK_ROOT}/emulator

# Install system image, platform, ndk and build tools
RUN sdkmanager "ndk;${ANDROID_NDK_VERSION}" "cmake;3.10.2.4988404"
RUN sdkmanager "platforms;android-30"
RUN sdkmanager "build-tools;30.0.3"