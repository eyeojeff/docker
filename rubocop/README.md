# RuboCop Docker Images

The [RubCop](https://github.com/rubocop-hq/rubocop) software is a static code
analyzer for the [Ruby](https://www.ruby-lang.org/) language based on the
[community style guide](https://github.com/rubocop-hq/ruby-style-guide).
The [Docker](https://www.docker.com/) resources herein are used to assemble
Docker container images with the RubCop software pre-installed.
